﻿using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesseract;

namespace extSp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ChosenFile = "";
            openFileDialog1.InitialDirectory = @"C:\Users\massimo\Documents\Visual Studio 2015\Projects\extSp\extSp\bin\Debug\tmp";
            openFileDialog1.FileName = "";
            //you can add .jpeg , etc.
            openFileDialog1.Filter = "pdf(*.pdf)|*.pdf|ALL Files(*.*)|*.*";
            ChosenFile = openFileDialog1.FileName;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    elab(openFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Errore: " + ex.Message);
                }
            }
        }


        List<object[]> SData;
        void elab(string file)
        {
            Console.WriteLine("\n\n Elab: " + file);

            //convert to image (low res)
            ConvertSingleImage(file, 150);

            //Check Logo 
            ////TemplateMatch Ref = matchAllFilters(page, logo);
            //match(file + ".png", logo, 1);
            ////Delete files
            //delete(file + ".png");

            //Get Specimen data
            //convert to image (med res)
            ConvertSingleImage(file, 300);
            //Check Blobs
            SData = findSpecimenbyBlobs(file + ".png");

            //Save file
            treeView1.Nodes.Clear();
            int c = 0;
            foreach (object[] d in SData)
            {
                treeView1.Nodes.Add("("+c+")"+(string)d[3]);
                c++;
            }
            //saveblobToFile(d);

            //Delete files
            //delete(file + ".png");

        }


        void match(string source, string template, int filterlevel = 1)
        {
            System.Drawing.Bitmap img_temp;

            //Get image and release
            using (System.Drawing.Bitmap img_source = (Bitmap)Bitmap.FromFile(source))
            {
                img_temp = new Bitmap(img_source);
            }
            System.Drawing.Bitmap img_template = (Bitmap)Bitmap.FromFile(template);

            // GreyScale
            Grayscale filterg = new Grayscale(0.2125, 0.7154, 0.0721);
            img_temp = filterg.Apply(img_temp);
            img_template = filterg.Apply(img_template);

            //Invert
            //Invert filteri = new Invert();
            //img_source = filteri.Apply(img_source);
            //img_template = filteri.Apply(img_template);

            // Select Binarization & Similarity level
            //SISThreshold filterbin = new SISThreshold();    //0.877
            //StuckiDithering filterbin = new StuckiDithering();  //0.906
            //FloydSteinbergDithering filterbin = new FloydSteinbergDithering();  //0.908
            //BayerDithering filterbin = new BayerDithering();  //0.919
            float Sim = 1f;
            switch (filterlevel)
            {
                default:
                case 1:
                    SISThreshold filterSIS = new SISThreshold();
                    img_template = filterSIS.Apply(img_template);
                    img_temp = filterSIS.Apply(img_temp);
                    Sim = 0.870f;
                    break;
                case 2:
                    StuckiDithering filterStuck = new StuckiDithering();
                    img_template = filterStuck.Apply(img_template);
                    img_temp = filterStuck.Apply(img_temp);
                    Sim = 0.905f;
                    break;
                case 3:
                    FloydSteinbergDithering filterFloyd = new FloydSteinbergDithering();
                    img_template = filterFloyd.Apply(img_template);
                    img_temp = filterFloyd.Apply(img_temp);
                    Sim = 0.907f;
                    break;
                case 4:
                    BayerDithering filterBayer = new BayerDithering();
                    img_template = filterBayer.Apply(img_template);
                    img_temp = filterBayer.Apply(img_temp);
                    Sim = 0.915f;
                    break;
            }

            //Set Search area
            double w = img_temp.Width * 0.5;
            double h = img_temp.Height * 0.15;
            Rectangle searchZone = new Rectangle(0, 0, (int)w, (int)h);


            //Matching
            ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(Sim);
            TemplateMatch[] matchings = tm.ProcessImage(img_temp, img_template, searchZone);

            if (matchings.Count() > 0)
            {
                //debug
                Console.WriteLine("Matchings " + matchings.Count() + " elements!");

                //Get Top Matching
                float top = 0;
                float worst = 1;
                foreach (TemplateMatch m in matchings)
                {
                    if (m.Similarity > top)
                    {
                        top = m.Similarity;
                        //bestMatch = m;
                    }
                    if (m.Similarity < worst)
                        worst = m.Similarity;
                }

                //Debug
                BitmapData data = img_temp.LockBits(
                        new Rectangle(0, 0, img_temp.Width, img_temp.Height),
                        ImageLockMode.ReadWrite, img_temp.PixelFormat);
                Drawing.Rectangle(data, searchZone, Color.Green);
                if (top != 0)
                {
                    //highlight found matchings
                    for (int c = 0; c < matchings.Count() && c < 50; c++)
                    {
                        Drawing.Rectangle(data, matchings[c].Rectangle, Color.Red);
                        Console.WriteLine(matchings[c].Similarity + " at " + matchings[c].Rectangle.Location.ToString() + " Dim: " + matchings[c].Rectangle.Width + "x" + matchings[c].Rectangle.Height);
                    }
                    //show images
                    //pictureBox1.Image = img_source;
                    //pictureBox2.Image = img_template;
                }
                img_temp.UnlockBits(data);

            }
            else
            {
                Console.WriteLine("No logo found!");
            }


            //return bestMatch;
        }

        List<object[]> findSpecimenbyBlobs(string source)
        {
            System.Drawing.Bitmap img_temp1;
            System.Drawing.Bitmap img_temp2;
            System.Drawing.Bitmap img_orig;

            using (System.Drawing.Bitmap img_source = (Bitmap)Bitmap.FromFile(source))
            {
                img_temp1 = new Bitmap(img_source);
                img_temp2 = new Bitmap(img_source);
                img_orig = new Bitmap(img_source);
            }

            // GreyScale
            Grayscale filterg = new Grayscale(0.2125, 0.7154, 0.0721);
            img_temp1 = filterg.Apply(img_temp1);

            // Erode
            Erosion filter = new Erosion();
            // apply filter 2 times
            img_temp1 = filter.Apply(img_temp1);
            img_temp2 = (Bitmap)img_temp1.Clone();
            img_temp1 = filter.Apply(img_temp1);
            img_temp1 = filter.Apply(img_temp1);

            //Binarization
            SISThreshold filterSIS = new SISThreshold();
            img_temp1 = filterSIS.Apply(img_temp1);
            img_temp2 = filterSIS.Apply(img_temp2);

            //Inversion
            Invert filterI = new Invert();
            img_temp2 = filterI.Apply(img_temp2);

            // create an instance of blob counter algorithm
            BlobCounterBase bc = new BlobCounter();
            // set filtering options
            bc.FilterBlobs = true;
            bc.MinWidth = 1500;
            bc.MinHeight = 250;
            // set ordering options
            bc.ObjectsOrder = ObjectsOrder.Size;
            // process binary image
            bc.ProcessImage(img_temp1);
            Blob[] blobs = bc.GetObjectsInformation();
            Console.WriteLine("Blobs count: " + blobs.Count());

            //List per salvare i dati trovati!
            // si compone cosi:
            // 0- (bitmap)  immagine dei dati del cliente (a sinistra della firma)
            // 1- (bitmap)  immagine della firma
            // 2- (bitmap)  immagine del cag (per OCR)
            // 3- (stringa) cag (post-OCR)
            // ...  per utilizzi interni
            //  4
            //  5
            List<object[]> SData = new List<object[]>();

            //Esamina Blobs
            int container_w = img_temp1.Width;
            int container_h = img_temp1.Height;
            foreach (Blob b in blobs)
            {
                //Escludi blob contenitore (l'immagine stessa)
                if (b.Rectangle.Width == container_w)
                { Console.WriteLine("Blob {0} is container", b.ID); continue; }

                //Filtra secondo le posizioni dei Rectangle
                // Width                
                //  da total_w*0.7 a total_w*0.9
                if (b.Rectangle.Width > container_w * 0.9)
                { Console.WriteLine("Blob {0} skipped due to width > 0.9", b.ID); continue; }
                if (b.Rectangle.Width < container_w * 0.65)
                { Console.WriteLine("Blob {0} skipped due to width < 0.65", b.ID); continue; }
                // Initial position
                //  x
                //  da 0 a total_w *0.1
                if (b.Rectangle.X > container_w * 0.1)
                { Console.WriteLine("Blob {0} skipped due to X pos > 0.1", b.ID); continue; }
                //  y
                //  da total_w *0.2 
                if (b.Rectangle.Y < container_h * 0.2)
                { Console.WriteLine("Blob {0} skipped due to Y pos < 0.2", b.ID); continue;}

                //Ok Blob firma trovato!
                Console.WriteLine("Working on Blob {0}:", b.ID);
                object[] curData = new object[6];
                //Salva immagini
                // => 0 DATI
                Crop filter_s0 = new Crop(b.Rectangle);
                curData[0] = filter_s0.Apply(img_orig);
                // => 1 FIRMA
                //  elimina parte del margine destra pagina (0.99 = taglia 1%)
                double page_rmargin = 1;
                int f_w = Convert.ToInt32((img_orig.Width * page_rmargin) - (b.Rectangle.X + b.Rectangle.Width));
                int f_h = Convert.ToInt32(b.Rectangle.Height);
                int f_x = Convert.ToInt32((img_orig.Width * page_rmargin) - f_w);
                int f_y = Convert.ToInt32(b.Rectangle.Y);
                //  lascia margine intorno alla firma (1.10 = 10%)
                double f_margin = 1.10;
                int f_w_m = Convert.ToInt32(f_w * f_margin);
                int f_h_m = Convert.ToInt32(f_h * f_margin);
                int f_x_m = Convert.ToInt32((img_orig.Width * page_rmargin) - f_w_m);
                int f_y_m = Convert.ToInt32(b.Rectangle.Y - ((f_h_m - f_h) / 2));
                //  estrai
                Rectangle s = new Rectangle(f_x_m, f_y_m, f_w_m, f_h_m);
                Crop filter_s1 = new Crop(s);
                curData[1] = filter_s1.Apply(img_orig);
                // => 2 CAG
                int c_w = Convert.ToInt32(b.Rectangle.Width * 0.15);
                int c_h = Convert.ToInt32(b.Rectangle.Height * 0.145);
                int c_x = Convert.ToInt32((b.Rectangle.X + b.Rectangle.Width) - c_w);
                int c_y = Convert.ToInt32((b.Rectangle.Y + b.Rectangle.Height) - c_h);
                Rectangle s3 = new Rectangle(c_x, c_y, c_w , c_h );
                Crop filter_s3 = new Crop(s3);
                curData[2] = cropText(filter_s3.Apply(img_orig));
                // => ... Dati per future elaborazioni (rimossi alla fine)
                curData[4] = filter_s1.Apply(img_temp2);  //Salva immagine poco erosa per pulizia
                curData[5] = marginText((Bitmap)curData[2]);  //Salva CAG con margine per OCR

                //Clean
                //pulisci immagine firma, tutti gli elementi prima del campo firma e di piccole dimensioni vengono rimossi
                BlobCounterBase blbclean = new BlobCounter();
                blbclean.FilterBlobs = true;
                blbclean.MaxWidth = Convert.ToInt32(f_w * 0.1);
                blbclean.MaxHeight = Convert.ToInt32(f_h * 0.1);
                // create convex hull searching algorithm
                GrahamConvexHull hullFinder = new GrahamConvexHull();
                blbclean.ObjectsOrder = ObjectsOrder.XY;
                blbclean.ProcessImage((Bitmap)curData[4]);
                Blob[] blbclean_blbs = blbclean.GetObjectsInformation();
                Console.WriteLine("   Cleaning : Blobs count: " + blbclean_blbs.Count());
                Rectangle blb_f = new Rectangle(f_x-f_x_m, f_y- f_y_m, f_w, f_h);
                foreach (Blob blbclean_blb in blbclean_blbs)
                {
                    //Salta se è il blob container
                    if (blbclean_blb.Rectangle.Width == f_w_m)
                    { Console.WriteLine("   Cleaning : Blob {0} is container", blbclean_blb.ID); continue; }

                    //Skip se all'interno dell'area di firma
                    if (blbclean_blb.Rectangle.X > blb_f.X &&
                        blbclean_blb.Rectangle.Y > blb_f.Y && blbclean_blb.Rectangle.Y < blb_f.Y+blb_f.Height)
                    { Console.WriteLine("   Cleaning : Blob {0} skipped due to X,Y > {1},{2}", blbclean_blb.ID, blb_f.X, blb_f.Y); continue;  }

                    //Skip se entra in contatto con l'area di firma
                    if (blbclean_blb.Rectangle.Right >= blb_f.X &&
                        blbclean_blb.Rectangle.Y >= blb_f.Y)
                    { Console.WriteLine("   Cleaning : Blob {0} skipped due to rectangle touching left area", blbclean_blb.ID); continue; }
                    if (blbclean_blb.Rectangle.X >= blb_f.X &&
                        blbclean_blb.Rectangle.Bottom >= blb_f.Y)
                    { Console.WriteLine("   Cleaning : Blob {0} skipped due to rectangle touching top area", blbclean_blb.ID); continue; }

                    // Ok scritta al di fuori del campo firma
                    Bitmap img_wrk = (Bitmap)curData[1];
                    BitmapData data = img_wrk.LockBits(
                        new Rectangle(0, 0, img_wrk.Width, img_wrk.Height),
                        ImageLockMode.ReadWrite, img_wrk.PixelFormat);
                        Drawing.FillRectangle(data, blbclean_blb.Rectangle, Color.White);
                    img_wrk.UnlockBits(data);

                    /*
                    //Polygons
                    // get polygons
                    List<IntPoint> leftPoints = new List<IntPoint>();
                    List<IntPoint> rightPoints = new List<IntPoint>();
                    List<IntPoint> edgePoints = new List<IntPoint>();
                    blbclean.GetBlobsLeftAndRightEdges(blbclean_blb, out leftPoints, out rightPoints);
                    edgePoints.AddRange(leftPoints); edgePoints.AddRange(rightPoints);
                    List<IntPoint> blbclean_blb_hull = hullFinder.FindHull(edgePoints);
                    // convert List PointF  (for Graphics)
                    PointF[] edgePointsF = new PointF[edgePoints.Count()];
                    for (int p=0; p<edgePoints.Count(); p++)
                    { edgePointsF[p]= new PointF(edgePoints[p].X, edgePoints[p].Y); }
                    GraphicsPath PolygonPath = new GraphicsPath();
                    PolygonPath.AddPolygon(edgePointsF);
                    // draw
                    BitmapData data = img_temp2.LockBits(
                                    new Rectangle(0, 0, img_temp2.Width, img_temp2.Height),
                                    ImageLockMode.ReadWrite, img_temp2.PixelFormat);
                        //Drawing.FillRectangle(data, blbclean_blb.Rectangle, Color.White);
                        Drawing.Polygon(data, blbclean_blb_hull, Color.LightGreen);
                    img_temp2.UnlockBits(data);
                    //curData[1] = img_temp2;

                    //fill polygons
                    //only in not original image (Graphics limitation)
                    Graphics G = Graphics.FromImage((Bitmap)curData[1]);
                    G.FillPath(new SolidBrush(Color.Red), PolygonPath);
                    */
                }

                // 3 Recognize data
                string curStr = "";
                //Tesseract 2
                tessnet2.Tesseract ocr = new tessnet2.Tesseract();
                ocr.SetVariable("tessedit_char_whitelist", "A01/0123456789"); // If digit only
                //ocr.SetVariable("user_patterns_suffix", "user-patterns");
                ocr.SetVariable("language_model_penalty_non_dict_word", "0");
                ocr.SetVariable("language_model_penalty_non_freq_dict_word", "0");
                ocr.Init(@"C:\Users\massimo\Documents\Visual Studio 2015\Projects\extSp\extSp\bin\Debug\tessdata2", "ita", false); // To use correct tessdata
                List<tessnet2.Word> result = ocr.DoOCR((Bitmap)curData[5], Rectangle.Empty);
                //Debug Tesseract2
                Console.Write("Blob {0} : ", b.ID);
                foreach (tessnet2.Word word in result)
                { Console.Write("{0} ({1})", word.Text, word.Confidence); curStr += word.Text; }
                Console.Write("\n");
                curData[3] = curStr;

                //Tesseract 3
                //var engine = new TesseractEngine(@"C:\Users\massimo\Documents\Visual Studio 2015\Projects\extSp\extSp\bin\Debug\tessdata", "ita", EngineMode.Default);
                //engine.SetVariable("tessedit_char_whitelist", "A01/0123456789");
                ////engine.SetVariable("user_patterns_suffix", "user-patterns");
                //Page p = engine.Process((Bitmap)curData[2], PageSegMode.SingleLine);
                //curStr = p.GetText().ToString().Replace("\n", String.Empty);
                //float con = p.GetMeanConfidence();
                ////Debug
                //Console.WriteLine("Blob {0} : {1} ({2})", b.ID, curStr, con);
                //curData[3] = curStr;

                SData.Add(curData);
            }
            Console.WriteLine("Blobs saved: " + SData.Count());

            return SData;
        }

        private Bitmap cropText(Bitmap bitmap)
        {
            Bitmap img_orig = new Bitmap(bitmap);
            Bitmap img_temp = new Bitmap(bitmap);

            // GreyScale
            Grayscale filterg = new Grayscale(0.2125, 0.7154, 0.0721);
            img_temp = filterg.Apply(img_temp);

            // Erode (1 time)
            Erosion filter = new Erosion();
            img_temp = filter.Apply(img_temp);

            //Binarization
            OtsuThreshold filterbin = new OtsuThreshold();
            filterbin.ApplyInPlace(img_temp);

            //Inversion
            Invert filterinv = new Invert();
            filterinv.ApplyInPlace(img_temp);

            //Blob Counter
            BlobCounter bc = new BlobCounter();
            bc.ProcessImage(img_temp);
            Rectangle[] rects = bc.GetObjectsRectangles();
            Rectangle rect_big = new Rectangle(img_temp.Width, img_temp.Height,0,0);

            //Create containing Rect
            foreach (Rectangle rect in rects)
            {
                if (rect.X < rect_big.X)
                    rect_big.X = rect.X;
                if (rect.Y < rect_big.Y)
                    rect_big.Y = rect.Y;
                if (rect.X+rect.Width > rect_big.X+rect_big.Width)
                    rect_big.Width = (rect.X+rect.Width)-rect_big.X;
                if (rect.Y+rect.Height > rect_big.Y+rect_big.Height)
                    rect_big.Height = (rect.Y+rect.Height) -rect.Height;
            }
            //correct overflow
            if (rect_big.Width + rect_big.X > img_temp.Width)
                rect_big.Width = img_temp.Width - rect_big.X;
            if (rect_big.Height + rect_big.Y > img_temp.Height)
                rect_big.Height = img_temp.Height - rect_big.Y;

            //Return Cropped Bitmap
            Crop filter_crop = new Crop(rect_big);
            return filter_crop.Apply(img_orig);
        }

        Bitmap marginText(Bitmap bitmap)
        {
            //Create new Bitmap
            Bitmap result = new Bitmap(bitmap.Width + 100, bitmap.Height + 100);
            Graphics g = Graphics.FromImage(result);
            g.Clear(Color.White);
            g.DrawImage(bitmap, 50, 50);

            return result;
        }

        /*
        private Bitmap ocrErosion(Bitmap bitmap)
        {
            Bitmap img_temp = new Bitmap(bitmap);

            // GreyScale
            Grayscale filterg = new Grayscale(0.2125, 0.7154, 0.0721);
            img_temp = filterg.Apply(img_temp);

            // Binarization
            OtsuThreshold filterbin = new OtsuThreshold();
            filterbin.ApplyInPlace(img_temp);

            // Erosion
            Erosion filter = new Erosion();
            filter.Apply(img_temp);

            pictureBox1.Image = img_temp;

            return img_temp;
        }
        */

        void saveblobToFile(object[] data)
        {
            Bitmap first = (Bitmap) data[1];
            Bitmap second = Crop(SetImageOpacity((Bitmap) data[2], 1f));
            //Bitmap result = new Bitmap(first.Width, first.Height);
            //fix :
            Bitmap result = new Bitmap(Math.Max(first.Width, second.Width), Math.Max(first.Height, second.Height));
            Console.WriteLine(first.Width);
            Graphics g = Graphics.FromImage(result);
            g.DrawImageUnscaled(first, 0, 0);
            g.DrawImageUnscaled(second, first.Width-second.Width, first.Height - second.Height);

            pictureBox1.Image = (Bitmap)data[2]; //cag
            pictureBox2.Image = result; //firma
            pictureBox2.Top = pictureBox1.Top + pictureBox1.Height + 10;
            pictureBox3.Image = (Bitmap)data[0]; //dati
            pictureBox3.Top = pictureBox2.Top + pictureBox2.Height + 10;

            //result.Save("result.jpg");
        }
        public Bitmap SetImageOpacity(System.Drawing.Image image, float opacity)
        {
            try
            {
                //create a Bitmap the size of the image provided  
                Bitmap bmp = new Bitmap(image.Width, image.Height);
                //create a graphics object from the image  
                using (Graphics gfx = Graphics.FromImage(bmp))
                {
                    //create a color matrix object  
                    ColorMatrix matrix = new ColorMatrix();
                    //set the opacity  
                    matrix.Matrix33 = opacity;
                    //create image attributes  
                    ImageAttributes attributes = new ImageAttributes();
                    //set the color(opacity) of the image  
                    attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                    //now draw the image  
                    gfx.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
                }
                return bmp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }





        #region Debug-UI




        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }


        #endregion




        #region PDF Management

        private void ConvertSingleImage(string filename, int res)
        {
            libPDF conv = new libPDF();

            //Setup the converter
            conv.FirstPageToConvert = 1;
            conv.LastPageToConvert = 1;
            conv.FitPage = true;
            conv.OutputFormat = "png16m";
            conv.ResolutionX = res;
            conv.ResolutionY = res;
            System.IO.FileInfo input = new FileInfo(filename);
            string output = filename + ".png";
            //If the output file exists already, be sure to add a
            //random name at the end until it is unique!
            try
            {
                File.Delete(output);
            }
            catch (Exception ex)
            {

            }

            string arg = conv.ParametersUsed;

            if (conv.Convert(input.FullName, output) == true)
                Console.WriteLine(string.Format("{0} converted!", filename));
            else
                Console.WriteLine(string.Format("{0} NOT converted! Check Args! {1}", filename, arg));

        }

        #endregion


        #region File Management

        void delete(string file)
        {
            while (File.Exists(file))
            {
                try
                {
                    File.Delete(file);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Waiting to release file...");
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        #endregion

        public static Bitmap Crop(Bitmap bmp)
        {
            int w = bmp.Width;
            int h = bmp.Height;

            Func<int, bool> allWhiteRow = row =>
            {
                for (int i = 0; i < w; ++i)
                    if (bmp.GetPixel(i, row).R != 255)
                        return false;
                return true;
            };

            Func<int, bool> allWhiteColumn = col =>
            {
                for (int i = 0; i < h; ++i)
                    if (bmp.GetPixel(col, i).R != 255)
                        return false;
                return true;
            };

            int topmost = 0;
            for (int row = 0; row < h; ++row)
            {
                if (allWhiteRow(row))
                    topmost = row;
                else break;
            }

            int bottommost = 0;
            for (int row = h - 1; row >= 0; --row)
            {
                if (allWhiteRow(row))
                    bottommost = row;
                else break;
            }

            int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col)
            {
                if (allWhiteColumn(col))
                    leftmost = col;
                else
                    break;
            }

            for (int col = w - 1; col >= 0; --col)
            {
                if (allWhiteColumn(col))
                    rightmost = col;
                else
                    break;
            }

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try
            {
                var target = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(bmp,
                      new RectangleF(0, 0, croppedWidth, croppedHeight),
                      new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                      GraphicsUnit.Pixel);
                }
                return target;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                  ex);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            int i = Convert.ToInt16(Regex.Match(treeView1.SelectedNode.ToString(), @"\(([^)]*)\)").Groups[1].Value);
            saveblobToFile(SData[i]);
        }
    }
}
