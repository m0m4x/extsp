﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace extSp
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                //Parsing parameters
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "--gui":
                            new Program().startGui();
                            break;
                        default:
                            Console.Write("Help:");
                            Console.Write("--gui => open Gui");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine( "Error: "+ex.Message+"\n"+ex.InnerException );
            }
        }

        void startGui()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

    }
}
